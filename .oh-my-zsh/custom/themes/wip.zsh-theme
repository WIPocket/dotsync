if [ $UID -eq 0 ]; then
  UCOLOR="red";
else
  UCOLOR="white";
  if [ $USER = wipocket ]; then UCOLOR="green"; fi
  if [ $USER = pi ]; then UCOLOR="green"; fi
fi

HCOLOR="white";
if [ $HOST = wip-deb ]; then HCOLOR="green"; fi
if [ $HOST = raspberrypi ]; then HCOLOR="yellow"; fi
if [ $HOST = localhost ]; then HCOLOR="yellow"; fi

local return_code="%(?..%{$fg[red]%}%?%{$reset_color%})"

PROMPT='%* %{$terminfo[bold]$fg[$UCOLOR]%}%n%{$fg[$HCOLOR]%}@%m\
%{$reset_color%}%{$terminfo[bold]$fg[blue]%}%~%{$reset_color%}\
$([ -e .altname ] && printf " " && cat .altname && printf " ")\
%(!.#.$)%  '
PROMPT2='%{$fg[red]%}\ %{$reset_color%}'
RPS1='${return_code} $(ps -o etime $$ | tail -n 1 | tr -d " \n")'
