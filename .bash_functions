function o {
	opener "$*" &
}

function gac {
	git status
	git commit -a -m "$*"
}

function ganc {
	git status
	git add .
	git commit -a -m "$*"
}

function facmoddl {
	echo Downloading $*
	git clone $* facmoddl --depth=1
	cd facmoddl
	NEWNAME=$(jq ".name" -r info.json)_$(jq ".version" -r info.json)
	echo Mod folder name: "$NEWNAME"
	cd ..
	mv facmoddl "$NEWNAME"
}

function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
    return 1
 else
    for n in $@
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}

function big {
	echo "\`\`\`$(toilet "$*")\`\`\`"| clipcopy
}

function bigfnt {
	echo "\`\`\`$(toilet -f ascii12 "$*")\`\`\`"| clipcopy
}

function bigbigfnt {
	echo "\`\`\`$(toilet -f bigascii12 "$*")\`\`\`"| clipcopy
}

function bigfuture {
	echo "\`\`\`\n$(toilet -f future "$*")\`\`\`"| clipcopy
}

function bigblock {
	echo "\`\`\`$(toilet -f mono12 "$*")\`\`\`"| clipcopy
}

function wide {
	echo "$(toilet -f wideterm "$*")"| clipcopy
}

function dec {
    clipMsg="$(xclip -o | grep -Pzo -m 1 "(?s)-----BEGIN PGP MESSAGE-----.+?-----END PGP MESSAGE-----")"
    if [[ -n "$clipMsg" ]]; then
        gpg --decrypt <<<"$clipMsg"
    else
        gpg --decrypt
    fi
}

function mcd {
  mkdir -p -- $1
  cd $1
}

function csrun {
	mcs $1.cs && mono $1.exe
}

function otp {
  cd ~/OTP
  andotp_gencode $(ls -t | grep "otp_" | head -n 1) $1 | grep '[0-9]' | clipcopy
  popd
}

function sssh {
  # try to connect every 0.5 secs (modulo timeouts)
  while true; do command ssh "$@"; [ $? -ne 255 ] && break || sleep 0.5; done
}

function uname {
  /bin/uname $* | sed -E 's/amd|x86/kredenc/g'
}
