alias -- -='cd -'
alias ..=up
alias -g ...=../..
alias -g ....=../../..
alias -g .....=../../../..
alias -g ......=../../../../..
alias //='#'
alias 1='cd -'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'
alias NOBRIGHT='sudo sh -c "echo 100 > /sys/class/backlight/intel_backlight/brightness"'
alias _='sudo '
alias a.='atom .'
alias ab='sudo apt-build'
alias afind='ack -il'
alias ans='upd && sudo apt autoremove'
alias arem='sudo apt autoremove'
alias aslp='upd && sudo apt autoremove && slp'
alias aslpe='aslp && exit'
alias b='upower -i /org/freedesktop/UPower/devices/battery_BAT0| grep -E "state|\ to\ |percentage"'
alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0'
alias ccat='highlight -O xterm256'
alias chrome='/usr/local/lib/node_modules/puppeteer/.local-chromium/linux-722234/chrome-linux/chrome --no-sandbox &'
alias cls=_clearTermBottom
alias cun='genuname cm | clipcopy'
alias d='date '\''+Locale date:%n%t%c %nDay of week:%n%t%A %nAmerican style date:%n%t%D %nNormal style date:%n%t%d. %m. %Y %nTime:%n%t%T %nTime zone:%n%t%Z (%:::z) %nUnix time (seconds):%n%t%s'\'
alias d69='node /media/wipocket/extbd/fromWin/node/discordRPC/index.js'
alias dc=cd
alias ddg='elinks duckduckgo.com'
alias diff='diff --color'
alias dl=youtube-dl
alias egrep='egrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}'
alias et='echo $TERM'
alias fac='goto fac && . rununmodded.sh'
alias facc='goto fac && . mod && bin/x64/factorio --load-game "saves/_Creative.zip"'
alias ff='x-www-browser>/dev/null 2>/dev/null&'
alias fgrep='fgrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}'
alias ft='fbterm "/bin/sh -c '\''tmux a || tmux'\''"'
alias g=goto
alias ga='git add'
alias gc='git checkout'
alias gd='git diff'
alias genuname='node /media/wipocket/extbd/fromWin/node/username/gen.js'
alias gip='wget https://api.ipify.org -O - -q&&echo'
alias gl='git log --all --graph --decorate --format="%C(yellow)%h %Cgreen%an%Creset %s %Cred%d%Creset"'
alias globurl='noglob urlglobber '
alias gm='goto ng; cd savedData; vlc --loop --intf=curses 677111_God-Mode.mp3 722594_Troglodyte-Original-Mix.mp3 965133_Rutra---Vulture--Remasteri.mp3 721720_Ragedance.mp3; popd'
alias gp='git push'
alias gpl='git pull'
alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}'
alias gs='git status'
alias h=hr
alias hbright='sudo sh -c "echo 937 > /sys/class/backlight/intel_backlight/brightness"'
alias hh='HOME=$(realpath .)'
alias history=omz_history
alias jabbanew='jabba use openjdk@1.14.0'
alias jpvpn='sudo protonvpn c --cc JP'
alias k=/media/wipocket/extbd/fromWin/node/k/index.js
alias kerio='sudo /etc/init.d/kerio-kvc'
alias keriofix='kerio stop && kerio start && ping sssvt.cz -c 8 | grep " 0% packet loss" || keriofix'
alias killbg='kill ${${(v)jobstates##*:*:}%=*}'
alias knmap='ping 192.168.191.241 -c 1 | head -n 2 && nmap 192.168.191.241 -p 25565,34197 -Pn 2> /dev/null | tail -n 6'
alias l='ls -lhAt'
alias la='ls -lAh'
alias lbright='sudo sh -c "echo 100 > /sys/class/backlight/intel_backlight/brightness"'
alias ll='ls -l'
alias lo=locate
alias ls=_ls
alias lsa='ls -lah'
alias m=micro
alias mbright='sudo sh -c "echo 450 > /sys/class/backlight/intel_backlight/brightness"'
alias md=mkdir
alias mine='jabba use openjdk@1.14.0 && java -jar /media/wipocket/Windows/Users/Adam/Downloads/SkaiaCraft_Launcher/Mac\ and\ Linux/SkaiaCraft\ Launcher\ v3.0.jar'
alias mmc='/home/wipocket/.jabba/jdk/adopt-openj9@1.8.0-242/bin/java -jar /media/wipocket/Windows/Users/Adam/Downloads/SkaiaCraft_Launcher/Mac\ and\ Linux/SkaiaCraft\ Launcher\ v3.0.jar'
alias mwin='sudo mount /dev/sda2 /media/wipocket/Windows'
alias nb='jabba use default && java -jar /media/wipocket/Windows/Users/Adam/Desktop/nargbox/.nargbox\ 1.4.7/MagicLa*.jar'
alias nbs='cd /media/wipocket/Windows/Users/Adam/Desktop/nargbox/nargbox_1.4.7/Server/;./start.sh'
alias neopeč=neofetch
alias nfac='ping 192.168.194.104 | tee /dev/stderr | grep -m 1 "64 bytes" && /media/wipocket/extbd/fromWin/factorio/1.1.30/factorio/bin/x64/factorio --mp-connect 192.168.194.104'
alias ng='goto ng; ./vlc.sh'
alias ngf='goto ng; cd fav; ./vlc.sh'
alias nh='HOME=/home/wipocket'
alias nnmap='ping 192.168.194.104 -c 1 | head -n 2 && nmap 192.168.194.104 -p 25565,34197 -Pn 2> /dev/null | tail -n 6'
alias nps='source $ZSH_CUSTOM/themes/wip.zsh-theme'
alias o.='o .'
alias optjpeg='/opt/mozjpeg/bin/jpegtran -copy none -optimize -progressive -outfile'
alias p=popd
alias pgpkey='gpg --armor --export FB01751476FE880D60E0CBAC3C8207ED1C3146F8 | clipcopy'
alias qr='qrencode -t ansi'
alias r=/home/wipocket/scripts/r
alias rbt='systemctl reboot'
alias rd=rmdir
alias realvmplayer=/lib/vmware/bin/vmplayer
alias rsr='source ~/.zshrc'
alias rt='r 1'
alias run-help=man
alias rzt='sudo systemctl restart zerotier-one.service'
alias salias='alias -L > ~/.bash_aliases'
alias sc=systemctl
alias si='z;r 0 q'
alias sl='echo no'
alias slowtype='pv -qL'
alias slp='systemctl suspend'
alias sps='PS1='\''$ '\'';RPS1='
alias stf='mv /tmp'
alias swttr='curl -s wttr.in | head -n 7 | tail -n 5'
alias t=todo.sh
alias tf='cd $(mktemp -d)'
alias tg=telegram-cli
alias tl='t ls'
alias togglescreen='xrandr --listactivemonitors | grep eDP-1 && xrandr --output eDP-1 --off || xrandr --output eDP-1 --auto'
alias tp='t pri'
alias tsc='ts -C'
alias upd='sudo apt update && sudo apt upgrade'
alias vl=physlock
alias vls='vl&systemctl suspend'
alias vluac='vlc --intf=lua'
alias vpnoff='sudo protonvpn d'
alias w='goto wigle && node . && cd'
alias wan='goto wigle;node analisis;p'
alias which-command=whence
alias wifiRepeat='sudo create_ap wlo1 wlo1 "ｗｉｄｅ" "itsRlyWideNGL"'
alias win='cd /media/wipocket/Windows/Users/Adam/'
alias wttr='curl '\''wttr.in?qF'\'
alias xmlcat='xmllint --format'
alias xnolaptop='xrandr --output eDP-1 --off'
alias z=/home/wipocket/scripts/z
alias zs='ps u'
