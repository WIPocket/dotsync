#/bin/bash

SRC=$PWD
cd $HOME
ln -f -s $SRC/.bash_aliases .bash_aliases
ln -f -s $SRC/.zshrc .zshrc
ln -f -s $SRC/.bash_functions .bash_functions
ln -f -s $SRC/.bashrc .bashrc
ln -f -s $SRC/.oh-my-zsh .oh-my-zsh
ln -f -s $SRC/.bash_logout .bash_logout
