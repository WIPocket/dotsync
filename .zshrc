[ $ASCIINEMA_REC ] && return;

HIDET=1

if tty | grep tty; then
	setfont CyrSlav-VGA14
	unicode_start
fi

if test $TERM = "xterm"; then
  TERM="xterm-256color"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="wip"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)

plugins=(colored-man-pages command-not-found node sudo themes zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# Update every second
#TMOUT=1

#TRAPALRM() {
#    zle reset-prompt
#}



#source $ZSH/custom/zsh-autocomplete/zsh-autocomplete.plugin.zsh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.bash_functions ]; then
	. ~/.bash_functions
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

[ -s "/home/wipocket/.jabba/jabba.sh" ] && source "/home/wipocket/.jabba/jabba.sh"
jabba use adopt-openj9@1.8.0-242


# Source goto
[[ -s "/usr/local/share/goto.sh" ]] && source /usr/local/share/goto.sh

source ~/.config/up/up.sh

PATH=${PATH}:~/.local/bin:~/scripts:/home/wipocket/.linuxbrew/bin

source $(dirname $(gem which colorls))/tab_complete.sh

source ~/scripts/initJSEnv.sh

function _clearTermBottom {
  clear
  echo -e "\033[50000;0H"
  [ -z "$HIDET" ] && todo.sh ls
  zle reset-prompt 2> /dev/null
  echo > /dev/null
}

zle -N _clearTermBottom
bindkey '^L' _clearTermBottom
_clearTermBottom

function _kbhome { cd;zle reset-prompt }
zle -N _kbhome
bindkey '^H' _kbhome

function _kbup { cd ..;zle reset-prompt }
zle -N _kbup
bindkey '^U' _kbup

function _kbpopd { popd > /dev/null;zle reset-prompt }
zle -N _kbpopd
bindkey '^P' _kbpopd

function _kbcdminus { cd -;zle reset-prompt }
zle -N _kbcdminus
bindkey '^O' _kbcdminus

function _kbmagicls { echo; \ls --color; zle reset-prompt }
zle -N _kbmagicls
bindkey '^K' _kbmagicls

function _kbresetprompt { zle reset-prompt }
zle -N _kbresetprompt
bindkey '^B' _kbresetprompt

function _kbtoggleprompt {
  if [[ -z $RPS1 ]] then
    nps
  else
    sps
  fi
  zle reset-prompt
}
zle -N _kbtoggleprompt
bindkey '^Z' _kbtoggleprompt

function _ls {
  if [[ -z "$*" ]] then
    echo no
  else
    /bin/ls --color $*
  fi
}
alias ls=_ls

tl | head -n 4
echo
